import { Component, OnInit } from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Router } from "@angular/router"


import * as $ from 'jquery' 

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    isCollapsed = true;
  
    
    
    kaka(): void{
      if (screen.width<=800 && this.isCollapsed)
      {
        
        
      $(".slider2s").hide()   
      }     
    
  
    }
  
    
    
    createLogoutMessage(): void {
      this.message.success('You are logged out successfully !', {
        nzDuration: 2000
      });}
  
    constructor(private message: NzMessageService,  private router: Router) { 
     
    }
    open(){
      this.isCollapsed=!this.isCollapsed;
      if (screen.width<=800 && this.isCollapsed)
      {
        
        
      $(".slider2s").hide()   
      }   
      else{
        $(".slider2s").show() 
      }
    }
  
    close(){
      this.isCollapsed=true;
      if (screen.width<=800 && this.isCollapsed)
      {
        
        
      $(".slider2s").hide()   
      }  
  
    }
    doLogout(){
      localStorage.removeItem("username");
      this.router.navigate(['login'])
      this.createLogoutMessage()
    }
  
    ngOnInit(): void {
   
  this.kaka();
      
      
  
    }
  

}
