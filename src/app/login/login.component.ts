import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzMessageService } from 'ng-zorro-antd/message';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  validateForm!: FormGroup;
  isForgetEmailVisible = false;
  phone: string = null;
  password: string = null;
  idd=true;
  

    createLoginMessage(): void {
      this.message.success('You are logged in successfully !', {
        nzDuration: 2000
      });}
      
        
          
    constructor(private fb: FormBuilder, private router: Router, private message: NzMessageService) {}

  ngOnInit(): void {
    if(localStorage.getItem('username')!= null){
      this.router.navigate(['dashboard/createproduct'])

      ;
    }
   
    this.validateForm = this.fb.group({
      phone: [null, [ Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });


  }
    
    async login() {
      for (const i in this.validateForm.controls) {
        this.validateForm.controls[i].markAsDirty();
        this.validateForm.controls[i].updateValueAndValidity();
      }
      if(this.phone!=undefined && this.phone.length>9)
      {
        localStorage.setItem('username',"admin");

        this.router.navigate(['dashboard/createproduct'])
        this.createLoginMessage();
      }
      
    }
   
  
  isVisible = false;
  showModal(): void {
  this.isVisible = true;
}

handleOk(): void {
  console.log('Button ok clicked!');
  this.isVisible = false;
}

handleCancel(): void {
  console.log('Button cancel clicked!');
  this.isVisible = false;
}
}
