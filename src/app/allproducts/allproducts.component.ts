import { Component, OnInit } from '@angular/core';
import { kilofarmsApiService } from "src/app/services/kilofarmsapi.service";
import { Router } from '@angular/router';


interface DataItem {
  skuId: string;
    skuName: string;
}
@Component({
  selector: 'app-allproducts',
  templateUrl: './allproducts.component.html',
  styleUrls: ['./allproducts.component.css']
})
export class AllproductsComponent implements OnInit {
public objall = [];
public objalls = [];
  searchValue = '';
  visible = false;
  visibleid = false;



  reset(): void {
    this.searchValue = '';
    this.search();
  }

  search(): void {
    this.objall=this.objalls;
    this.objall = this.objall.filter((item: DataItem) => item.skuName.toLowerCase().indexOf(this.searchValue.toLowerCase()) !== -1);
  }

  searchValueid = '';
  


  resetid(): void {
    this.searchValueid = '';
    this.searchid();
  }

  searchid(): void {
    this.objall=this.objalls;
    this.objall = this.objall.filter((item: DataItem) => item.skuId.indexOf(this.searchValueid) !== -1);
  }
  transfer(some:string): void {
    this._kilofarmsApiService.fetch(some);

    this.router.navigate(['dashboard/productbyid'])
  }

  constructor( private _kilofarmsApiService: kilofarmsApiService, private router:Router) {

  }

  async ngOnInit() {
    await this._kilofarmsApiService.getall()
    .subscribe
    (
      res=>
      {
        this.objall = res.data;
        this.objalls = res.data;
        console.log(this.objalls);
      }
    )
  }
  
}
