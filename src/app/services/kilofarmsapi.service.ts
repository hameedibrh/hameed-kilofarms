import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import {CreateProductclass} from 'src/app/classes/createproductclass'
import {AllProductsclass} from 'src/app/classes/allproductsclass'
import {Productbyidclass} from 'src/app/classes/productbyidclass'

var id = "";

@Injectable()
export class kilofarmsApiService {
    constructor(private httpclient: HttpClient){} 
    
    
    post(opost:CreateProductclass): Observable<any>{
        new HttpHeaders({
            'content-type': 'application/json',
        })
            return this.httpclient.post("https://cc2pruxs38.execute-api.us-east-1.amazonaws.com/staging/vegetables", opost);
        
    }

    getall(): Observable<any>{
      
            return this.httpclient.get<any>("https://6j57eve9a1.execute-api.us-east-1.amazonaws.com/staging/vegetable?item=all&userId=114118037");
        
    }

   fetch(idd: string){
       id=idd;
   }
    

    getbyid(): Observable<any>{
        let paramas1 = new HttpParams().set('item', id)
       .set('userId', '114118037');
        return this.httpclient.get("https://6j57eve9a1.execute-api.us-east-1.amazonaws.com/staging/vegetable",{params:paramas1});
    
}

}