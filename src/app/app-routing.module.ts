import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { ProductbyidComponent } from './productbyid/productbyid.component';
import { AllproductsComponent } from './allproducts/allproducts.component';
import { DashboardComponent } from './dashboard/dashboard.component'
import { CreateproductComponent } from './createproduct/createproduct.component'
import { AuthGuard } from './auth.guard';
import { ErrorpageComponent } from './errorpage/errorpage.component'



const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'login', component: LoginComponent, },
  {path: 'dashboard', component: DashboardComponent, canActivate : [AuthGuard], children:[
    {path: '', redirectTo: 'createproduct', pathMatch: 'full'},
    {path: 'createproduct', component: CreateproductComponent},
    {path: 'productbyid', component: ProductbyidComponent},
    {path: 'allproducts', component: AllproductsComponent}
  ]
  },
  { path: "**", component:ErrorpageComponent}

  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
