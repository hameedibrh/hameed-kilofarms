import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { kilofarmsApiService } from "src/app/services/kilofarmsapi.service";
import {CreateProductclass} from 'src/app/classes/createproductclass';
import { NzMessageService } from 'ng-zorro-antd/message';


@Component({
  selector: 'app-createproduct',
  templateUrl: './createproduct.component.html',
  styleUrls: ['./createproduct.component.css']
})
export class CreateproductComponent implements OnInit {
productname:string;
category:string;
unit:string;
canreload = false;

  validateForm!: FormGroup;

  async submitForm(): Promise<void> {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();

    }

    if(this.productname!= undefined && this.category!=undefined && this.unit!=undefined)
    {

   
    var opost = new CreateProductclass();
    opost.skuName= this.validateForm.value.productname;
    opost.user_id= '114118037';
    opost.skuUnit= this.validateForm.value.unit;
    opost.skuCategory= this.validateForm.value.category;


    await this._kilofarmsApiService.post(opost)
    .subscribe
    (
      data=>
      {
        console.log(data);
        this.createSuccessMessage();
        this.canreload=true;
    

      }
    )
    if (this.canreload=true){
      setTimeout(()=>{ location.reload() }, 1000);
    }
  }
  }

  unitChange(value: string): void {
  }
  categoryChange(value: string): void {
  }

  createSuccessMessage(): void {
      this.message.success('Data successfully inserted to the Database', {
        nzDuration: 2000
      });}

  constructor(private fb: FormBuilder, private _kilofarmsApiService: kilofarmsApiService, private message: NzMessageService) {}

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      productname: [null, [Validators.required]],
      category: [null, [Validators.required]],
      unit: [null, [Validators.required]]

    });

}
}
