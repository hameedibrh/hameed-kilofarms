import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { KilofarmsNgZorroAntdModule } from './ng-zorro-antd.module';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { AllproductsComponent } from './allproducts/allproducts.component';
import { ProductbyidComponent } from './productbyid/productbyid.component';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { ScrollingModule } from '@angular/cdk/scrolling';

import { NZ_ICONS } from 'ng-zorro-antd/icon';
import { IconDefinition } from '@ant-design/icons-angular';
import * as AllIcons from '@ant-design/icons-angular/icons';




import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormGroup } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CreateproductComponent } from './createproduct/createproduct.component'
import { ErrorpageComponent } from './errorpage/errorpage.component'

import { kilofarmsApiService } from './services/kilofarmsapi.service'

import {AllProductsclass} from 'src/app/classes/allproductsclass'
import { AuthGuard } from './auth.guard';



registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AllproductsComponent,
    ProductbyidComponent,
    DashboardComponent,
    CreateproductComponent,
    ErrorpageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    KilofarmsNgZorroAntdModule,
    ReactiveFormsModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }, kilofarmsApiService,  AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
