import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { kilofarmsApiService } from "src/app/services/kilofarmsapi.service";
import { Router } from '@angular/router';


interface DataItem {
  skuId: string;
    skuName: string;
    skuUnit: string;
    skuCategory: string
    skuImage: string
}
@Component({
  selector: 'app-productbyid',
  templateUrl: './productbyid.component.html',
  styleUrls: ['./productbyid.component.css']
})
export class ProductbyidComponent implements OnInit {
  public objall = [];
  public objalls = [];
  searchValue = '';
  visible = false;

  reset(): void {
    this.searchValue = '';
    this.search();
  }

  back(){
    this.router.navigate(['dashboard/allproducts'])

  }

  search(): void {
    this.objall=this.objalls;
    this.objall = this.objall.filter((item: DataItem) => item.skuName.toLowerCase().indexOf(this.searchValue.toLowerCase()) !== -1);
  }

  constructor( private _kilofarmsApiService: kilofarmsApiService, private router:Router) {}

  async ngOnInit(){
    await this._kilofarmsApiService.getbyid()
    .subscribe
    (
      res=>
      {
        this.objall = res.data;
        this.objalls = res.data;
        console.log(this.objalls);
      }
    )
  }
}
