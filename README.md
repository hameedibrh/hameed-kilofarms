# Kilofarms - Hameed Ibrahim

This project was created by Hameed Ibrahim (114118037) from NIT, Trichy for Kilofarms Intern Hiring!

In case of any queries, feel free to contact +91-6385540655 or Email me at hameedibrh@gmail.com.

## See the Working App without downloading the repo on my AWS Amplify

Visit this AWS Amplify Link to see the running web-app: [https://master.d1vwu2tlzwv5z3.amplifyapp.com/](https://master.d1vwu2tlzwv5z3.amplifyapp.com/)

## To login into the Dashboard

Enter any 10 digit mobile number and any random input as password to login (as mentioned in the Problem Statement).

## User ID Used in the API

User ID used in the API is my roll number which is 114118037.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.1.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
